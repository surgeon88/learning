class SampleMail < MailForm::Base
  attributes :name, :email, :message
  attributes :nickname
  validates :nickname, absence: true

  before_deliver do
    evaluated_callbacks << :before
  end
  after_deliver do
    evaluated_callbacks << :after
  end

  def evaluated_callbacks
    @evaluated_callbacks ||= []
  end

  def headers
    { from: self.email, to: 'you@domain.com' }
  end
end
