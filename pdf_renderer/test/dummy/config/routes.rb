Dummy::Application.routes.draw do
  resources :contact_forms, only: [:create, :new]
  root to: "contact_forms#new"
end
